import random

def generate_beans():
    beans = []
    unused = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    for i in range (9):
        rand = unused[random.randint(0, len(unused) - 1)]
        beans.append(rand)
        unused.remove(rand)
    #print(beans)
    return beans

def pairs(x):
    pairs = []
    for i in range (len(x) - 3):
        pairs.append((x[i], x[i + 3]))
    #print(pairs)
    return pairs

def input_guess():
    guess = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    unused = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    x = 0
    for i in range (3):
        line = input("podaj " + str(i + 1) + " rząd: ")
        while(len(line) == 0 or line.isdigit() == False):
            line = input("podaj " + str(i + 1) + " rząd jeszcze raz: ")
        for j in range (3):
            cor = False
            while cor == False or len(line) != 3 or len(line) == 0 or line.isdigit() == False:
                if int(line[j]) in unused:
                    guess[j + x] = int(line[j])
                    unused.remove(int(line[j]))
                    cor = True
                else:
                    if j == 1:
                        unused.append(int(line[0]))
                    if j == 2:
                        unused.append(int(line[0]), int(line[1]))
                    line = input("podaj " + str(i + 1) + " rząd jeszcze raz: ")
                    while(len(line) == 0 or line.isdigit() == False):
                        line = input("podaj " + str(i + 1) + " rząd jeszcze raz: ")
        x += 3
    print(guess)
    return guess

def save_score(file_name, score):
    imie = input("podaj swoje imię: ")
    with open(file_name, 'a') as file:
        file.write(imie + " ")
        file.write(str(score) + "\n")



def sort_scores(scores):
    for i in range (len(scores)):
        for j in range (len(scores) - i - 1):
            if int(scores[j][1]) > int(scores[j + 1][1]):
                tmp = scores[j]
                scores[j] = scores[j + 1]
                scores[j + 1] = tmp
    return scores
