from fasole_funkcje import generate_beans, pairs, input_guess, sort_scores, save_score

beans = generate_beans()

correct = 0
score = 0
while(correct < 9):
    guess = input_guess()
    guess_pairs = pairs(guess)

    correct = 0
    for i in range(9):
        if beans[i] == guess[i]:
            correct += 1

    beans_pairs = pairs(beans)
    correct_pairs = 0
    for i in range (6):
        if guess_pairs[i] in beans_pairs:
            correct_pairs += 1
            beans_pairs.remove(guess_pairs[i])
    
    print("fasole w dobrych miejscach:", correct)
    print("poprawne pionowe pary:", correct_pairs)
    score += 1
print("gratulacje, twój wynik to:", score)

save_score("high_scores_fasole.txt", score)

scores = []
with open("high_scores_fasole.txt", 'r') as file:
    for line in file:
        scores.append(line.split())

scores = sort_scores(scores)

print("najlepsze wyniki: ")
for i in range(len(scores)):
    print(scores[i][0] + ":", scores[i][1])