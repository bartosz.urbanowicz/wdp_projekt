import pygame
import random
from pygame.locals import *
from sys import exit
from fasole_funkcje import save_score, sort_scores

def generate_level():
    bullets.empty()
    enemies.empty()
    for i in range(random.randint(1, 5)):
        enemies.add(Enemy())

class Player(pygame.sprite.Sprite): 
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((10, 10))
        self.image.fill('white')
        self.rect = self.image.get_rect(center = (400, 200))
        self.cooldown = 0
        self.stats = {'hp': 5, 'xp': 0}
        self.immune = 0
        self.immune_time = 0

    def go_through_door(self, door_name):
        if door_name == 'up':
            self.rect.y += 180
        if door_name == 'right':
            self.rect.x -= 180
        if door_name == 'down':
            self.rect.y -= 180
        if door_name == 'left':
            self.rect.x += 180

        global check_if_tutorial
        check_if_tutorial = 0


    def player_movement(self):
        key_pressed_is = pygame.key.get_pressed()
        wall_collisions = []
        if pygame.sprite.spritecollide(self, walls, False):
            for i in range(len(pygame.sprite.spritecollide(self, walls, False))):
                wall_collisions.append(pygame.sprite.spritecollide(self, walls, False)[i].name)
        if key_pressed_is[K_d] and 'right' not in wall_collisions:
            self.rect.x += spd
        if key_pressed_is[K_a] and 'left' not in wall_collisions:
            self.rect.x -= spd
        if key_pressed_is[K_w] and 'up' not in wall_collisions:
            self.rect.y -= spd
        if key_pressed_is[K_s] and 'down' not in wall_collisions:
            self.rect.y += spd
        
        if pygame.sprite.spritecollide(self, doors, False):
            self.go_through_door(pygame.sprite.spritecollide(self, walls, False)[0].name)
            generate_level()

    def player_shooting(self):
        key_pressed = pygame.key.get_pressed()
        if self.cooldown == 0:
            if key_pressed[K_UP]:
                bullets.add(Bullet('up', self.rect.midtop))
                self.cooldown = 25
            if key_pressed[K_RIGHT]:
                bullets.add(Bullet('right', self.rect.midright))
                self.cooldown = 25
            if key_pressed[K_DOWN]:
                bullets.add(Bullet('down', self.rect.midbottom))
                self.cooldown = 25
            if key_pressed[K_LEFT]:
                bullets.add(Bullet('left', self.rect.midleft))
                self.cooldown = 25
        elif self.cooldown > 0:
            self.cooldown -= 1
    
    def check_if_hit(self):
        for sprite in pygame.sprite.spritecollide(self, bullets, False):
            bullet_positions = [self.rect.midtop, self.rect.midright, self.rect.midbottom, self.rect.midleft]
            if sprite.shooter not in bullet_positions and self.immune == 0:
                self.stats['hp'] -= 1
                self.immune = 1
                self.image.fill('gray')
                self.immune_time = 50
        
    def immunity(self):
        self.immune_time -= 1
        if self.immune_time < 0:
            self.immune = 0
            self.image.fill('white')
    
    def display_stats(self):
        hp_surf = font.render(f"HP: {self.stats['hp']}", True, 'white')
        hp_rect = hp_surf.get_rect(center = (100, 150))
        screen.blit(hp_surf, hp_rect)

        lvl_surf = font.render(f"XP: {self.stats['xp']}", True, 'white')
        lvl_rect = hp_surf.get_rect(center = (100, 250))
        screen.blit(lvl_surf, lvl_rect)

    def update(self):
        self.player_movement()
        self.player_shooting()
        self.check_if_hit()
        self.immunity()
        self.display_stats()

class Wall(pygame.sprite.Sprite):
    def __init__(self, type):
        super().__init__()
        if type == 'up':
            size = (200, 5)
            pos = (305, 100)
            self.name = 'up'
        if type == 'right':
            size = (5, 200)
            pos = (500, 105)
            self.name = 'right'
        if type == 'down':
            size = (200, 5)
            pos = (300, 300)
            self.name = 'down'
        if type == 'left':
            size = (5, 200)
            pos = (300, 100)
            self.name = 'left'

        self.image =  pygame.Surface(size)
        self.image.fill('white')
        self.rect = self.image.get_rect(topleft = pos)

class Door(pygame.sprite.Sprite):
    def __init__(self, type):
        super().__init__()
        if type == 'up':
            size = (20, 5)
            pos = (390, 100)
            self.name = 'up'
        if type == 'right':
            size = (5, 20)
            pos = (500, 190)
            self.name = 'right'
        if type == 'down':
            size = (20, 5)
            pos = (390, 300)
            self.name = 'down'
        if type == 'left':
            size = (5, 20)
            pos = (300, 190)
            self.name = 'left'
        self.image =  pygame.Surface(size)
        self.image.fill((32, 32, 32))
        self.rect = self.image.get_rect(topleft = pos)

class Bullet(pygame.sprite.Sprite):
    def __init__(self, dir, shooter):
        super().__init__()
        self.shooter = shooter
        self.image = pygame.Surface((3, 3))
        self.image.fill('white')
        self.rect = self.image.get_rect(center = shooter)
        self.dir = dir
    
    def update(self):
        if self.dir == 'up':
            self.rect.y -= 4
        if self.dir == 'right':
            self.rect.x += 4
        if self.dir == 'down':
            self.rect.y += 4
        if self.dir == 'left':
            self.rect.x -= 4

        if pygame.sprite.spritecollide(self, walls, False):
            self.kill()

class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.base_cooldown = random.randint(40, 60)
        self.cooldown = self.base_cooldown
        all_shooting_dirs = ['up', 'right', 'down', 'left']
        self.shooting_dirs = []
        for i in range (random.randint(1, 4)):
            rand_dir = random.choice(all_shooting_dirs)
            self.shooting_dirs.append(rand_dir)
            all_shooting_dirs.remove(rand_dir)
        self.image = pygame.Surface((10, 10))
        self.image.fill('red')
        self.rect = self.image.get_rect(center = (random.randint(310, 495), random.randint(110, 295)))
    
    def check_dead(self):
        for sprite in pygame.sprite.spritecollide(self, bullets, False):
            bullet_positions = [self.rect.midtop, self.rect.midright, self.rect.midbottom, self.rect.midleft]
            if sprite.shooter not in bullet_positions:
                self.kill()
                player.sprite.stats['xp'] += 1

    def enemy_shooting(self):
        if self.cooldown == 0:
            if 'up' in self.shooting_dirs:
                bullets.add(Bullet('up', self.rect.midtop))
                self.cooldown = self.base_cooldown
            if 'right' in self.shooting_dirs:
                bullets.add(Bullet('right', self.rect.midright))
                self.cooldown = self.base_cooldown
            if 'down' in self.shooting_dirs:
                bullets.add(Bullet('down', self.rect.midbottom))
                self.cooldown = self.base_cooldown
            if 'left' in self.shooting_dirs:
                bullets.add(Bullet('left', self.rect.midleft))
                self.cooldown = self.base_cooldown
        elif self.cooldown > 0:
            self.cooldown -= 1
    
    def update(self):
        self.check_dead()
        self.enemy_shooting()

pygame.init()
w = 800
h = 400
screen = pygame.display.set_mode((w, h))
pygame.display.set_caption("dungeon shooter")
clock = pygame.time.Clock()
font = pygame.font.Font(None, 50)
small_font = pygame.font.Font(None, 30) 

game_active = True

bg_surf = pygame.Surface((w, h))
bg_surf.fill((32, 32, 32))

player = pygame.sprite.GroupSingle()
player.add(Player())

walls = pygame.sprite.Group()
walls.add(Wall('up'), Wall('right'), Wall('down'), Wall('left'))

doors = pygame.sprite.Group()
doors.add(Door('up'), Door('right'), Door('down'), Door('left'))

bullets = pygame.sprite.Group()

enemies = pygame.sprite.Group()

tutorial_surf1 = small_font.render("w, a, s, d = move", True, 'white')
tutorial_rect1 = tutorial_surf1.get_rect(center = (400, 20))
tutorial_surf2 = small_font.render("arrow keys = shoot", True, 'white')
tutorial_rect2 = tutorial_surf2.get_rect(center = (400, 45))
tutorial_surf3 = small_font.render("go through the door to enter the next room", True, 'white')
tutorial_rect3 = tutorial_surf3.get_rect(center = (400, 70))

spd = 1

for sprite in player:
    cooldown = 0

player_shooting_cooldown = 0

check_if_tutorial = 1

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

    if game_active:
        screen.blit(bg_surf, (0, 0))

        if check_if_tutorial == 1:
            screen.blit(tutorial_surf1, tutorial_rect1)
            screen.blit(tutorial_surf2, tutorial_rect2)
            screen.blit(tutorial_surf3, tutorial_rect3)

        enemies.draw(screen)
        enemies.update()
        player.draw(screen)
        player.update()
        walls.draw(screen)
        walls.update()
        doors.draw(screen)
        doors.update()
        bullets.draw(screen)
        bullets.update()
        player_shooting_cooldown -= 1

        if player.sprite.stats['hp'] <= 0:
            game_active = False
    
    else:
        screen.fill((32, 32, 32))
        game_over_surf1 = font.render("game over", True, 'white')
        game_over_rect1 = game_over_surf1.get_rect(center = (400, 150))
        game_over_surf2 = small_font.render("press space to restart", True, 'white')
        game_over_rect2 = game_over_surf2.get_rect(center = (400, 200))
        game_over_surf3 = small_font.render("press q to quit and save score", True, 'white')
        game_over_rect3 = game_over_surf3.get_rect(center = (400, 250))
        screen.blit(game_over_surf1, game_over_rect1)
        screen.blit(game_over_surf2, game_over_rect2)
        screen.blit(game_over_surf3, game_over_rect3)

        key_pressed = pygame.key.get_pressed()
        if key_pressed[K_SPACE]:
            player.sprite.stats['hp'] = 5
            player.sprite.stats['xp'] = 0
            check_if_tutorial = 1
            generate_level()
            enemies.empty()
            game_active = True
        if key_pressed[K_q]:
            pygame.quit()
            break

    pygame.display.update()
    clock.tick(60)

save_score("high_scores_shooter.txt", player.sprite.stats['xp'])

scores = []
with open("high_scores_shooter.txt", 'r') as file:
    for line in file:
        scores.append(line.split())

scores = sort_scores(scores)
scores.reverse()

print("najlepsze wyniki: ")
for i in range(len(scores)):
    print(scores[i][0] + ":", scores[i][1])