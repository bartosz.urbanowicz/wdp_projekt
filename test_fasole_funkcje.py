from fasole_funkcje import generate_beans, pairs, input_guess, save_score, sort_scores

def test_generate_beans():
    beans = generate_beans()
    for i in range (9):
        assert(beans[i] <= 9 and beans[i] >= 1)

def test_pairs():
    assert(pairs([1, 2, 3, 4, 5, 6, 7, 8, 9]) == [(1, 4), (2, 5), (3, 6), (4, 7), (5, 8), (6, 9)])

def test_input_guess(monkeypatch):
    inputs = iter(['123', '456', '789'])
    monkeypatch.setattr('builtins.input', lambda _: next(inputs))
    guess = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert(input_guess() == guess)

def test_save_score(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda _: 'bartek')
    save_score("high_scores_fasole.txt", 10)
    with open("high_scores_fasole.txt", 'r') as file:
        lines = file.readlines()
        last_line = lines[-1]
    assert(last_line == 'bartek 10\n')
    with open("high_scores_fasole.txt", 'w') as file:
        for line in lines:
            if line != 'bartek 10\n':
                file.write(line)

def test_sort_scores():
    scores = [('a', 1), ('c', 30), ('b', 2)]
    assert(sort_scores(scores) == [('a', 1),  ('b', 2), ('c', 30)])